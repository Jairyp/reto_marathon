package com.marathon.controller.web.dto;

import com.marathon.core.domain.Person;

import java.util.List;

public class PersonsWebDto {

    List<Person> personList;

    public PersonsWebDto(List<Person> personList) {
        this.personList = personList;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {
        this.personList = personList;
    }
}
