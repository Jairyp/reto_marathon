package com.marathon.controller.web;

import com.google.gson.Gson;
import com.marathon.controller.web.dto.PersonWebDto;
import com.marathon.controller.web.dto.PersonsWebDto;
import com.marathon.core.domain.Person;
import com.marathon.service.PersonService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

@ApiOperation(value = "/v1/person", tags = "person")
@RestController
@RequestMapping("/v1/person")
public class PersonController {

    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @ApiOperation(value = "consulta personas juridicas")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS",response = Person.class),
            @ApiResponse(code = 404, message = "NOT FOUND")
    })
    @GetMapping
    public HttpEntity<PersonsWebDto> getList() {

        List<Person> personList;
        try{
            personList = personService.getList();
            PersonsWebDto personsWebDto = new PersonsWebDto(personList);
            return new ResponseEntity<>(personsWebDto, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }


    }



    @ApiOperation(value = "Guardar persona juridica")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "SUCCESS",response = Person.class)
    })
    @PostMapping
    public HttpEntity<Person> create(@RequestBody PersonWebDto personWebDto)  {
        try {
            Person persona = getPersonaJuridica(personWebDto.getTipo(),personWebDto.getRuc());
            personService.save(persona);
            return new ResponseEntity<Person>(persona, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Person>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private Person getPersonaJuridica(String tipo,String ruc) throws Exception{
        StringBuilder json = new StringBuilder();

        URL url = new URL("http://wsruc.com/Ruc2WS_JSON.php?tipo="
                .concat(tipo)
                .concat("&ruc=")
                .concat(ruc)
                .concat("&token=cXdlcnR5bGFtYXJja19zYUBob3RtYWlsLmNvbXF3ZXJ0eQ=="));

        HttpURLConnection conexion = (HttpURLConnection) url.openConnection();
        conexion.setRequestMethod("GET");

        BufferedReader rd = new BufferedReader(new InputStreamReader(conexion.getInputStream()));
        String linea;

        while ((linea = rd.readLine()) != null) {
            json.append(linea);
        }
        rd.close();

        return convertJsonToObject(json.toString());
    }

    private Person convertJsonToObject(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, Person.class);
    }

}
