package com.marathon.repository;

import com.marathon.core.domain.Person;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

    private static final Logger log = LoggerFactory.getLogger(PersonRepositoryImpl.class);

    private final JdbcTemplate jdbcTemplate;
    private final PersonRowMapper personRowMapper;


    public PersonRepositoryImpl(JdbcTemplate jdbcTemplate, PersonRowMapper personRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.personRowMapper = personRowMapper;
    }

    @Override
    public List<Person> getList() {
        List<Person> list = jdbcTemplate.query("call GetAllPerson",personRowMapper);
        return list;
    }

    @Override
    public int save(Person person) {
        return jdbcTemplate.update("call PersonInsert(?,?,?,?,?,?,?,?);",new Object[] {
                person.getRuc(),
                person.getRazon_social(),
                person.getEstado(),
                person.getDireccion(),
                person.getUbigeo(),
                person.getDepartamento(),
                person.getProvincia(),
                person.getDistrito()
        });
    }


}
