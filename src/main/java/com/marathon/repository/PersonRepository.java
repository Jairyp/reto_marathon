package com.marathon.repository;

import com.marathon.core.domain.Person;

import java.util.List;

public interface PersonRepository {

    public List<Person> getList();
    int save(Person customer);

}
