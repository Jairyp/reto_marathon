package com.marathon.repository;

import com.marathon.core.domain.Person;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;


@Component
public class PersonRowMapper implements RowMapper<Person>{

    @Override
    public Person mapRow(ResultSet rs, int i) throws SQLException {

        Person person = new Person();
        person.setId(rs.getInt("id"));
        person.setRuc(rs.getString("ruc"));
        person.setRazon_social(rs.getString("razon_social"));
        person.setEstado(rs.getString("estado"));
        person.setDireccion(rs.getString("direccion"));
        person.setUbigeo(rs.getString("ubigeo"));
        person.setDepartamento(rs.getString("departamento"));
        person.setProvincia(rs.getString("provincia"));
        person.setDistrito(rs.getString("distrito"));
        return person;
    }
}
