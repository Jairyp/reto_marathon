package com.marathon.service;

import com.marathon.repository.PersonRepository;
import com.marathon.core.domain.Person;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    private PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public List<Person> getList() {
        return personRepository.getList();
    }

    @Override
    public void save(Person customer) {
        personRepository.save(customer);
    }

}
