package com.marathon.service;

import com.marathon.core.domain.Person;

import java.util.List;

public interface PersonService {
    List<Person> getList();
    void save(Person customer);

}
