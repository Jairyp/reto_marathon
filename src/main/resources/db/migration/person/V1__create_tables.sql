

CREATE TABLE IF NOT EXISTS person (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `ruc` VARCHAR(11) NOT NULL,
    `razon_social` VARCHAR(100) NOT NULL,
    `estado` VARCHAR(20) NOT NULL,
    `direccion` VARCHAR(400) NOT NULL,
    `ubigeo` VARCHAR(20) NOT NULL,
    `departamento` VARCHAR(20) NOT NULL,
    `provincia` VARCHAR(20) NOT NULL,
    `distrito` VARCHAR(20) NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY  (`id`),
    INDEX `person_idx` (`id` ASC)
    );

