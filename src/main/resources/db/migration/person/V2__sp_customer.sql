DELIMITER //

CREATE PROCEDURE GetAllPerson()
BEGIN
SELECT *  FROM person;
END //

CREATE PROCEDURE PersonInsert (IN p_ruc varchar(11),
                               IN p_razon_social varchar(100),
                               IN p_estado varchar(20),
                               IN p_direccion varchar(400),
                               IN p_ubigeo varchar(20),
                               IN p_departamento varchar(20),
                               IN p_provincia varchar(20),
                               IN p_distrito varchar(20)
                    )
BEGIN
insert into person (ruc,razon_social,estado,direccion,ubigeo,departamento,provincia,distrito)
values(p_ruc,p_razon_social,p_estado,p_direccion,p_ubigeo,p_departamento,p_provincia,p_distrito);
END //


DELIMITER ;