# Reto Marathon

## Descripción
Crear un microservicio el cual gestione la informacion de una persona juridica y almacena los datos en mysql

## Objetivo
* Crear proyectos usando: 
    * https://start.spring.io/
    * maven
      ```
      mvn archetype:generate -DgroupId=com.marathon -DartifactId=marathon -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
      ``` 

* Como usarlo de Manera Local     
  * ubicarse en la ruta base del proyecto donde se encuentra el archivo \reto_marathon\docker-compose.yml
  * Luego ejecutar el siguiente comando esto levantara el microservicio con la base de datos
    ```
      docker-compose up --build
      ``` 
  * validar que los contenedores esten corriendo con el siguiente comando, posterior validacion se puede consumir el microservicio en localhost:8085     

    ```
      docker ps -a
      ``` 
  
![Screenshot](imagen_docker.png)

* Spring boot:
    * Arquitectura (Controller, Service, Repository, Entity y Dto)

    
## Reto 
* crear 2 endpoints para guardar y consultar la informacion de la persona juridica:
    * POST /v1/customers
    * GET /v1/customers
    
## Prerequisites
**Software**
* Java SDK 8.x or higher
* Maven 3.x or higher
* Docker 20.10.x or higher
* Docker Compose: 2.2.3 or higher
* IntelliJ IDEA
